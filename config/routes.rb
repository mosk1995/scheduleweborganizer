Rails.application.routes.draw do
  get 'subjects/index'

  get 'subjects/show'

  get 'subjects/new'

  get 'subjects/create'

  get 'subjects/edit'

  get 'subjects/update'

  get 'subjects/destroy'

  get 'timed_objects/index'

  get 'timed_objects/show'

  get 'timed_objects/new'

  get 'timed_objects/create'

  get 'timed_objects/edit'

  get 'timed_objects/update'

  get 'timed_objects/destroy'

  get 'schedule_days/index'

  get 'schedule_days/show'

  get 'schedule_days/new'

  get 'schedule_days/create'

  get 'schedule_days/edit'

  get 'schedule_days/update'

  get 'schedule_days/destroy'

  get 'schedule_days_controller/index'

  get 'schedule_days_controller/show'

  get 'schedule_days_controller/new'

  get 'schedule_days_controller/create'

  get 'schedule_days_controller/edit'

  get 'schedule_days_controller/update'

  get 'schedule_days_controller/destroy'

  root 'main_page#create'
  get 'main_page/create'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  resources :schedules

end
