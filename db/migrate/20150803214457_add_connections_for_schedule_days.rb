class AddConnectionsForScheduleDays < ActiveRecord::Migration
  def change
    #Вспомогательная таблица для связи дней расписания с событиями дня
    create_table :schedule_days_timed_objects, id: false do |t|
      t.belongs_to :schedule_day, index: true
      t.belongs_to :timed_object, index: true
    end
    #Связь между расписанием и его днями
    add_belongs_to :schedule_days, :schedule
  end
end
