class AddConnections < ActiveRecord::Migration

  def change
    #Вспомогательная таблица для связи пользователей с их группами
    create_table :users_user_groups, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :user_group, index: true
    end

    #Вспомогательная таблица для связи групп с доступными им расписаниями
    create_table :user_groups_schedules, id: false do |t|
      t.belongs_to :schedules, index: true
      t.belongs_to :user_group, index: true
    end

    # Связь между TimedObject и Subject

    add_belongs_to :timed_objects, :subject

  end

end
