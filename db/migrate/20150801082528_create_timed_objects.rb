class CreateTimedObjects < ActiveRecord::Migration
  def change
    create_table :timed_objects do |t|
      t.datetime :timeBegin
      t.datetime :timeEnd

      t.timestamps null: false
    end
  end
end
