class AddIndexes < ActiveRecord::Migration
  def change
    add_index :users, :name
    add_index :user_groups, :name
    add_index :schedules, :name
    add_index :timed_objects, :timeBegin
    add_index :subjects, :name
  end
end
