class CreateScheduleDays < ActiveRecord::Migration
  def change
    create_table :schedule_days do |t|
      t.integer :number
      t.datetime :date
      t.string :name

      t.timestamps null: false
    end
  end
end
