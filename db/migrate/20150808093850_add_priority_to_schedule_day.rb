class AddPriorityToScheduleDay < ActiveRecord::Migration
  def change
    add_column :schedule_days, :priority, :integer
  end
end
