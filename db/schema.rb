# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150808101555) do

  create_table "schedule_days", force: :cascade do |t|
    t.integer  "number"
    t.datetime "date"
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "schedule_id"
    t.integer  "priority"
  end

  create_table "schedule_days_timed_objects", id: false, force: :cascade do |t|
    t.integer "schedule_day_id"
    t.integer "timed_object_id"
  end

  add_index "schedule_days_timed_objects", ["schedule_day_id"], name: "index_schedule_days_timed_objects_on_schedule_day_id"
  add_index "schedule_days_timed_objects", ["timed_object_id"], name: "index_schedule_days_timed_objects_on_timed_object_id"

  create_table "schedules", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "type_schedule"
  end

  add_index "schedules", ["name"], name: "index_schedules_on_name"

  create_table "subjects", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "subjects", ["name"], name: "index_subjects_on_name"

  create_table "timed_objects", force: :cascade do |t|
    t.datetime "timeBegin"
    t.datetime "timeEnd"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "subject_id"
  end

  add_index "timed_objects", ["timeBegin"], name: "index_timed_objects_on_timeBegin"

  create_table "user_groups", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "user_groups", ["name"], name: "index_user_groups_on_name"

  create_table "user_groups_schedules", id: false, force: :cascade do |t|
    t.integer "schedules_id"
    t.integer "user_group_id"
  end

  add_index "user_groups_schedules", ["schedules_id"], name: "index_user_groups_schedules_on_schedules_id"
  add_index "user_groups_schedules", ["user_group_id"], name: "index_user_groups_schedules_on_user_group_id"

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "users", ["name"], name: "index_users_on_name"

  create_table "users_user_groups", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "user_group_id"
  end

  add_index "users_user_groups", ["user_group_id"], name: "index_users_user_groups_on_user_group_id"
  add_index "users_user_groups", ["user_id"], name: "index_users_user_groups_on_user_id"

end
