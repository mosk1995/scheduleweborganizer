# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create(name: 'user_1',password: '9889', password_confirmation: '9889')
UserGroup.create(name:'user_group_1')
Schedule.create(name:'schedule_1',description:'description_1', type_schedule:1)

Subject.create(name:'Working time')

to1=TimedObject.new(
    timeBegin:Time.zone.now.change({hour: 9, min:0, sec:0}).to_time,
    timeEnd:Time.zone.now.change({hour: 14, min:0, sec:0}).to_time,

)
to1.subject =Subject.first
to1.save

to2=TimedObject.new(
    timeBegin:Time.zone.now.change({hour: 15, min:0, sec:0}).to_time,
    timeEnd:Time.zone.now.change({hour: 21, min:0, sec:0}).to_time,
)
to2.subject =Subject.first
to2.save

for i in 1..6
  sd1=ScheduleDay.new(number:i, date:nil, name:nil, priority:1)
  sd1.schedule=Schedule.first
  sd1.timed_objects<<to1<<to2
  sd1.save
end