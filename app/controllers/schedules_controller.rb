class SchedulesController < ApplicationController

  #Вывод всех расписаний из базы данных (Название(с ссылкой) и Описание)
  def index
    @schedules = Schedule.all
  end

  #Отображение выбранного расписания (скорее всего сюда будет дописываться ограничение, или нет)
  def show
    @schedule = Schedule.find(params[:id])
  end

  #Подготовка к созданию нового расписания (пока только создание имени и описания)
  def new
    @schedule=Schedule.new
  end

  #Создание (обработка запроса POST)
  def create
    @schedule = Schedule.new (schedule_params)
    if @schedule.save
      redirect_to @schedule
    else
      render 'new'
    end
  end

  #Обновление расписания
  def edit
    @schedule=Schedule.find(params[:id])
  end

  def update
    @schedule=Schedule.find(params[:id])
    if @schedule.update (schedule_params)
      render 'edit'
    else
      render 'edit'
    end
  end

  #Удаление расписания
  def destroy

  end

  private
  def schedule_params
    params.require(:schedule).permit(:name, :description, :type_schedule)
  end
end
