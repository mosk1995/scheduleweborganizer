class ScheduleDay < ActiveRecord::Base
  belongs_to :schedule, inverse_of: :schedule_days
  has_and_belongs_to_many :timed_objects

  #  Нужно ввести свойство priority которое характеризует приоритет объекта описывающего день: 1- обычный объект; 2... - особые события,
  # из событий с одинаковым приоритетом (такой ситуации лучше постараться избежать на этапе валидации) выбирать первое, чем больше число тем выше приоритет

end
