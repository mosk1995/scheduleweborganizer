class UserGroup < ActiveRecord::Base
  validates :name, uniqueness: true
  has_and_belongs_to_many :users
  has_and_belongs_to_many :schedules
end
