class User < ActiveRecord::Base
  validates :name, uniqueness: true, presence: true
  has_and_belongs_to_many :user_groups
  has_secure_password


end
