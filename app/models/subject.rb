class Subject < ActiveRecord::Base
  validates :name, uniqueness: true, presence: true
  has_many :timed_object
end
