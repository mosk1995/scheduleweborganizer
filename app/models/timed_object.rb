class TimedObject < ActiveRecord::Base
  belongs_to :subject
  has_and_belongs_to_many :schedule_days
end
